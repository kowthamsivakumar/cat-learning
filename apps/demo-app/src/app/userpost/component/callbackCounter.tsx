import React, { useReducer } from 'react';
import { Form, Input, Container, Button } from 'semantic-ui-react';
import { userReducer } from '../reducer/UserPost';

function callbackCounter({ state, callback }: any) {
  console.log('callback render');
  return (
    <>
      <div style={{ padding: '30px' }}>
        <div className="card">
          <div style={{ textAlign: 'center', margin: '20px' }}>
            <h3>USECALLBACK</h3>
            <h1>{state}</h1>
          </div>

          <div
            style={{
              display: 'flex',
              justifyContent: 'center',
              alignItems: 'center',
            }}
          >
            <button className="button-primary button" onClick={callback}>
              Increment
            </button>
            {/* <button
              className="button-primary button"
              onClick={() => dispatch({ type: 'INCREMENT_BY_FIVE' })}
            >
              Increment-by-five
            </button>
            <button
              className="button-warning button"
              onClick={() => dispatch({ type: 'DECREMENT' })}
            >
              Decrement
            </button>
            <button
              className="button-warning button"
              onClick={() => dispatch({ type: 'DECREMENT_BY_FIVE' })}
            >
              Decrement-by-five
            </button> */}
          </div>
        </div>
      </div>
    </>
  );
}

export default React.memo(callbackCounter);
