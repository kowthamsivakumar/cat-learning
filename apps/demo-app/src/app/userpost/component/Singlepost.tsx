import React, { useEffect, useState } from 'react';
import { USER } from '../types/UserPost';
import { Grid, Card, Icon } from 'semantic-ui-react';

interface IProps {
  match: any;
  userPost: USER;
  getSinglePost: (id: number) => void;
}

const Singlepost: React.FC<IProps> = ({ match, getSinglePost, userPost }) => {
  useEffect(() => getSinglePost(match.params.id), []);

  return (
    <>
      <div
        style={{
          display: 'flex',
          justifyContent: 'center',
          alignItems: 'center',
          marginTop: '20px',
        }}
      >
        <Card>
          <Card.Content header={userPost.userPost.title} />
          <Card.Content description={userPost.userPost.body} />
          <Card.Content extra>
            <Icon name="user" />4 Friends
          </Card.Content>
        </Card>
      </div>
    </>
  );
};

export default Singlepost;
