import React from 'react';
import {
  Form,
  Input,
  Container,
  Button,
  Divider,
  Placeholder,
  Segment,
  Grid,
  Modal,
  Card,
  Header,
  Icon,
} from 'semantic-ui-react';
import { USER } from '../types/UserPost';
import Todo from './Todo';
import { SharedUiReact } from '@mylearning/shared/ui-react';
import UserPost from '../container/Userpost';

interface IProps {
  userPost: USER;
  fetchTodoRequest: () => void;
  postData: (data: any) => void;
  getAlbumCount: () => void;
  getCommentCount: () => void;
  getUserCount: () => void;
  getPostCount: () => void;
  deletePost: (id: number) => void;
}

const Home: React.FC<IProps> = ({
  userPost,
  fetchTodoRequest,
  postData,
  getAlbumCount,
  getCommentCount,
  getUserCount,
  getPostCount,
  deletePost,
}) => {
  const [open, setOpen] = React.useState(false);
  const [todo, setTodo] = React.useState(['one']);
  const [value, setValue] = React.useState('');

  React.useEffect(() => {
    fetchTodoRequest();
    getAlbumCount();
    getCommentCount();
    getUserCount();
    getPostCount();
  }, []);

  const handleTodo = () => {
    setTodo([...todo, value]);
  };

  const onChnage = (event: any) => {
    setValue(event.target.value);
  };

  return (
    <>
      <div>
        <div style={{ margin: '10px' }}>
          <SharedUiReact />
        </div>

        <Grid columns={4} style={{ padding: '10px' }}>
          <Grid.Column>
            <Card>
              <Card.Content>
                <Card.Header>
                  {userPost.totalComments}
                  <Header as="h2" floated="right">
                    <Icon name="comment alternate" color="blue" />
                  </Header>
                </Card.Header>

                <Card.Meta>Co-Worker</Card.Meta>
              </Card.Content>
            </Card>
          </Grid.Column>
          <Grid.Column>
            <Card>
              <Card.Content>
                <Card.Header>
                  {userPost.totalAlbum}
                  <Header as="h2" floated="right">
                    <Icon name="file" color="blue" />
                  </Header>
                </Card.Header>
                <Card.Meta>Total Album</Card.Meta>
              </Card.Content>
            </Card>
          </Grid.Column>
          <Grid.Column>
            <Card>
              <Card.Content>
                <Card.Header>
                  {userPost.totalpost}
                  <Header as="h2" floated="right">
                    <Icon name="list" color="blue" />
                  </Header>
                </Card.Header>
                <Card.Meta>Total Post</Card.Meta>
              </Card.Content>
            </Card>
          </Grid.Column>
          <Grid.Column>
            <Card>
              <Card.Content>
                <Card.Header>
                  {userPost.totalUsers}
                  <Header as="h2" floated="right">
                    <Icon name="users" color="blue" />
                  </Header>
                </Card.Header>
                <Card.Meta>Total User</Card.Meta>
              </Card.Content>
            </Card>
          </Grid.Column>
        </Grid>
        {/* <Grid
          style={{ height: "100vh" }}
          verticalAlign="middle"
          columns={2}
          centered
        >
          <Grid.Column>
            <Form>
              <Form.Field>
                <label>Name</label>
                <Input type="text" onChange={onChnage} />
              </Form.Field>
              <Form.Field>
                <label>Second Name</label>
                <Input type="text" />
              </Form.Field>
              <Form.Field>
                <label>Emp id</label>
                <Input type="text" />
              </Form.Field>
              <Divider />
              <Button floated="right" primary onClick={handleTodo}>
                On Submit
              </Button>

              <Form.Field>
                <Modal
                  open={open}
                  onOpen={() => setOpen(true)}
                  onClose={() => setOpen(false)}
                  trigger={
                    <Button floated="right" primary>
                      Sumbit
                    </Button>
                  }
                >
                  <Modal.Content>
                    Search for the keywords to learn more about each error.
                  </Modal.Content>
                  <Modal.Actions>
                    <Button floated="right" onClick={() => setOpen(false)}>
                      Cancel
                    </Button>
                    <Button floated="right" primary>
                      Sumbit
                    </Button>
                  </Modal.Actions>
                </Modal>

                <Button floated="right" onClick={() => setOpen(false)}>
                  Cancel
                </Button>
              </Form.Field>
            </Form>
          </Grid.Column>
        </Grid>
        <Todo todo={todo} /> */}

        <UserPost />
      </div>
    </>
  );
};

export default Home;
