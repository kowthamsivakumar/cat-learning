import { all, fork } from 'redux-saga/effects';
import todoSaga from './handler/user';
import UserSaga from './handler/userdetails';

export function* rootSaga() {
  yield all([fork(todoSaga), fork(UserSaga)]);
}
