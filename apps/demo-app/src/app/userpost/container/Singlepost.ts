import React from 'react';
import { connect } from 'react-redux';
import { RootState } from '../common/store';
import Singlepost from '../component/Singlepost';
import {
  fetchTodoRequest,
  postData,
  getAlbumCount,
  getCommentCount,
  getPostCount,
  getUserCount,
  getSinglePost,
} from '../action/user';

const mapStateToProps = (state: RootState) => ({
  userPost: state.user,
});

const mapDispatchToProps = {
  fetchTodoRequest,
  getPostCount,
  postData,
  getAlbumCount,
  getCommentCount,
  getSinglePost,
};

export default connect(mapStateToProps, mapDispatchToProps)(Singlepost);
