import React, { useReducer, useState, useMemo } from 'react';
import { Form, Input, Container, Button } from 'semantic-ui-react';
import { userReducer } from '../reducer/UserPost';

function CalculateRoot() {
  const [cal, setCal] = useState(0);

  const calMethod = (val: number) => {
    return val * 100;
  };

  const calmemo = useMemo(() => calMethod(cal), [cal]);

  return (
    <>
      <div style={{ padding: '30px' }}>
        <div className="card">
          <div style={{ textAlign: 'center', margin: '20px' }}>
            <h3>USEMEMO</h3>
            <h1>{cal}</h1>
            <h1>{calmemo}</h1>
          </div>

          <div
            style={{
              display: 'flex',
              justifyContent: 'center',
              alignItems: 'center',
            }}
          >
            <button
              className="button-primary button"
              onClick={() => {
                setCal(cal + 1);
              }}
            >
              Calculate
            </button>
            {/* <button
              className="button-primary button"
              onClick={() => dispatch({ type: 'INCREMENT_BY_FIVE' })}
            >
              Increment-by-five
            </button>
            <button
              className="button-warning button"
              onClick={() => dispatch({ type: 'DECREMENT' })}
            >
              Decrement
            </button>
            <button
              className="button-warning button"
              onClick={() => dispatch({ type: 'DECREMENT_BY_FIVE' })}
            >
              Decrement-by-five
            </button> */}
          </div>
        </div>
      </div>
    </>
  );
}

export default React.memo(CalculateRoot);
