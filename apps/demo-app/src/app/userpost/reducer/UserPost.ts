import { USER } from '../types/UserPost';

const initialState: USER = {
  userList: [
    {
      userId: 1,
      id: 1,
      title:
        'sunt aut facere repellat provident occaecati excepturi optio reprehenderit',
      body:
        'quia et suscipit\nsuscipit recusandae consequuntur expedita et cum\nreprehenderit molestiae ut ut quas totam\nnostrum rerum est autem sunt rem eveniet architecto',
    },
  ],
  totalComments: '',
  totalAlbum: '',
  totalUsers: '',
  totalpost: '',
  userPost: {
    userId: 0,
    id: 0,
    title: '',
    body: '',
  },
  toggleModal: false,
  loading: false,
};

export const userReducer = (state = initialState, action: any): USER => {
  switch (action.type) {
    case 'SET_ALL_POST':
      return { ...state, userList: action.payload };
    case 'SET_TOTAL_POST':
      return { ...state, totalpost: action.payload };
    case 'SET_TOTAL_ALBUM':
      return { ...state, totalAlbum: action.payload };
    case 'SET_TOTAL_COMMENTS':
      return { ...state, totalComments: action.payload };
    case 'SET_TOTAL_USERS':
      return { ...state, totalUsers: action.payload };
    case 'SET_SINGLE_POST':
      return { ...state, userPost: action.payload };
    case 'TOGGLE_MODAL':
      return { ...state, toggleModal: action.data };
    case 'TOGGLE_LOADING':
      return { ...state, loading: !state.loading };
    default:
      return state;
  }
};
