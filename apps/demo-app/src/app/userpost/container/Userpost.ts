import React from 'react';
import { connect } from 'react-redux';
import { RootState } from '../common/store';
import Userpost from '../component/UserPost';
import {
  fetchTodoRequest,
  postData,
  deletePost,
  toggleModal,
} from '../action/user';

const mapStateToProps = (state: RootState) => ({
  userPost: state.user,
});

const mapDispatchToProps = {
  fetchTodoRequest,
  postData,
  deletePost,
  toggleModal,
};

export default connect(mapStateToProps, mapDispatchToProps)(Userpost);
