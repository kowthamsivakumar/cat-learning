import axios from 'axios';

export const fetchList = () => {
  console.log('in call');
  return axios.get('https://jsonplaceholder.typicode.com/posts');
};

export const postData = (data: any) => {
  console.log('in call');
  return axios.post('https://jsonplaceholder.typicode.com/posts', data);
};

export const userCount = () => {
  console.log('in call');
  return axios.get('https://jsonplaceholder.typicode.com/users');
};

export const getAlbum = () => {
  console.log('in call');
  return axios.get('https://jsonplaceholder.typicode.com/albums');
};

export const getComment = () => {
  console.log('in call comment');
  return axios.get('https://jsonplaceholder.typicode.com/comments');
};

export const getpost = (id: number) => {
  return axios.get(`https://jsonplaceholder.typicode.com/posts/${id}`);
};

export const deletePostById = (id: number) => {
  return axios.delete(`https://jsonplaceholder.typicode.com/posts/${id}`);
};

export const getUsers = () => {
  return axios.get(`https://jsonplaceholder.typicode.com/users`);
};
