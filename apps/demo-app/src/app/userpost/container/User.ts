import React from 'react';
import { connect } from 'react-redux';
import { RootState } from '../common/store';
import Users from '../component/Users';
import { getUserList } from '../action/user';

const mapStateToProps = (state: RootState) => ({
  userList: state.userList,
});

const mapDispatchToProps = {
  getUserList,
};

export default connect(mapStateToProps, mapDispatchToProps)(Users);
