import React, { useEffect, useState } from 'react';
import { IUSER, USER } from '../types/User';
import { Link } from 'react-router-dom';
import { Grid, Card } from 'semantic-ui-react';

interface IProps {
  userList: IUSER;
  getUserList: () => void;
}

const Users: React.FC<IProps> = ({ userList, getUserList }) => {
  useEffect(() => getUserList(), []);
  console.log(userList);
  return (
    <>
      <div style={{ padding: '20px', height: '100vh' }}>
        <Grid columns={4}>
          {userList.userlist.map((user: USER) => (
            <Grid.Column>
              <Card
                link
                header={user.name}
                meta={user.email}
                description={user.company.catchPhrase}
                extra={user.address.city}
              />
            </Grid.Column>
          ))}
        </Grid>
      </div>
    </>
  );
};

export default Users;
