import React, { useReducer, useCallback, useState, useMemo } from 'react';
import { Form, Input, Container, Button } from 'semantic-ui-react';
import { userReducer } from '../reducer/UserPost';
import Callback from '../component/callbackCounter';
import Calculate from '../component/CalculateRoot';

const initialState = {
  counter: 0,
  callbackCounter: 0,
};

const counterReducer = (state = initialState, action: any) => {
  switch (action.type) {
    case 'DECREMENT':
      return { ...state, counter: state.counter - 1 };
    case 'INCREMENT':
      return { ...state, counter: state.counter + 1 };
    case 'INCREMENT_CALLBACK':
      return { ...state, callbackCounter: state.callbackCounter + 1 };
    case 'INCREMENT_BY_FIVE':
      return { ...state, counter: state.counter + 5 };
    case 'DECREMENT_BY_FIVE':
      return { ...state, counter: state.counter - 5 };
    default:
      return state;
  }
};

function Counter() {
  const [state, dispatch] = useReducer(counterReducer, initialState);

  const [counter, setCounter] = useState(0);

  const onClickCallBackCounter = useCallback(() => {
    setCounter(counter + 1);
  }, [counter]);

  const setCounterValue = () => {
    setCounter(counter + 1);
  };

  // const onClickMemoCounter = useMemo(() => {
  //   setCounterValue();
  // }, [counter]);

  console.log('parent render', state);

  return (
    <>
      <div style={{ height: '100vh', padding: '30px' }}>
        <div className="card">
          <div style={{ textAlign: 'center', margin: '20px' }}>
            <h1>{state.counter}</h1>
          </div>

          <div
            style={{
              display: 'flex',
              justifyContent: 'center',
              alignItems: 'center',
            }}
          >
            <button
              className="button-primary button"
              onClick={() => dispatch({ type: 'INCREMENT' })}
            >
              Increment
            </button>
            <button
              className="button-primary button"
              onClick={() => dispatch({ type: 'INCREMENT_BY_FIVE' })}
            >
              Increment-by-five
            </button>
            <button
              className="button-warning button"
              onClick={() => dispatch({ type: 'DECREMENT' })}
            >
              Decrement
            </button>
            <button
              className="button-warning button"
              onClick={() => dispatch({ type: 'DECREMENT_BY_FIVE' })}
            >
              Decrement-by-five
            </button>
          </div>
        </div>
        <Callback state={counter} callback={onClickCallBackCounter} />
        {/* <Callback state={counter} callback={onClickMemoCounter} /> */}
        <Calculate />
      </div>
    </>
  );
}

export default Counter;
