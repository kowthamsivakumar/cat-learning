import { USER_POST } from '../types/UserPost';

export const fetchTodoSuccess = (payload: USER_POST[]) => ({
  type: 'SET_ALL_POST',
  payload,
});

export const setUserCount = (payload: number) => ({
  type: 'SET_TOTAL_USERS',
  payload,
});

export const setSinglePost = (payload: any) => ({
  type: 'SET_SINGLE_POST',
  payload,
});

export const setAlbumCount = (payload: number) => ({
  type: 'SET_TOTAL_ALBUM',
  payload,
});

export const setCommentCount = (payload: number) => ({
  type: 'SET_TOTAL_COMMENTS',
  payload,
});

export const setPostCount = (payload: number) => ({
  type: 'SET_TOTAL_POST',
  payload,
});

export const getAlbumCount = () => ({
  type: 'GET_ALL_ALBUM_COUNT',
});

export const getSinglePost = (id: number) => ({
  type: 'GET_SINGLE_POST',
  id,
});

export const getUserCount = () => ({
  type: 'GET_ALL_USER_COUNT',
});

export const getCommentCount = () => ({
  type: 'GET_ALL_COMMENT_COUNT',
});

export const getPostCount = () => ({
  type: 'GET_ALL_POST_COUNT',
});

export const fetchTodoRequest = () => ({
  type: 'GET_ALL_POST',
});

export const deletePost = (id: number) => ({
  type: 'DELETE_POST',
  id,
});

export const postData = (data: any) => ({
  type: 'POST_USER',
  value: data,
});

export const closeModal = (data: boolean) => ({
  type: 'TOGGLE_MODAL',
  data,
});

export const toggleModal = (data: boolean) => ({
  type: 'TOGGLE_MODAL_CLOSE',
  data,
});

export const toggleLoader = () => ({
  type: 'TOGGLE_LOADING',
});

export const getUserList = () => ({
  type: 'GET_ALL_USER_LIST',
});

export const setUserList = (value: any) => ({
  type: 'SET_ALL_USER_LIST',
  value,
});
