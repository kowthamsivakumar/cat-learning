import { getUsers } from '../request/userpost';
import { setUserList } from '../../action/user';
import * as Eff from 'redux-saga/effects';

export function* fetchUsers(): any {
  try {
    // yield Eff.put(toggleLoader());
    const response = yield Eff.call(getUsers);
    yield Eff.put(setUserList(response.data));
    //yield Eff.put(toggleLoader());
  } catch (error) {
    // yield Eff.put(toggleLoader());
    console.log('error', error);
  }
}

function* UserSaga() {
  yield Eff.all([Eff.takeEvery('GET_ALL_USER_LIST', fetchUsers)]);
}

export default UserSaga;
