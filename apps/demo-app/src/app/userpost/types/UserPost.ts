export interface USER_POST {
  userId: number;
  id: number;
  title: string;
  body: string;
}

export interface USER {
  userList: USER_POST[];
  totalComments: string;
  totalUsers: string;
  totalpost: string;
  totalAlbum: string;
  userPost: USER_POST;
  toggleModal: boolean;
  loading: boolean;
}
