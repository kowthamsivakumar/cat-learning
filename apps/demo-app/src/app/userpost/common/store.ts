import { combineReducers, applyMiddleware } from 'redux';
import { userReducer } from '../reducer/UserPost';
import { userListReducer } from '../reducer/User';
import { composeWithDevTools } from 'redux-devtools-extension';

export const rootReducer = combineReducers({
  user: userReducer,
  userList: userListReducer,
});

export type RootState = ReturnType<typeof rootReducer>;
