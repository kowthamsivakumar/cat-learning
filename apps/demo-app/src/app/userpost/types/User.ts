export interface USER {
  id: number;
  name: string;
  username: string;
  email: string;
  address: IADDRESS;
  phone: string;
  website: string;
  company: ICOMPANY;
}

export interface ICOMPANY {
  name: string;
  catchPhrase: string;
  bs: string;
}

export interface IADDRESS {
  street: string;
  suite: string;
  city: string;
  zipcode: string;
  geo: {
    lat: string;
    lng: string;
  };
}

export interface IUSER {
  userlist: USER[];
  loading: boolean;
}
