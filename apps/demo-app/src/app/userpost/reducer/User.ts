import { IUSER } from '../types/User';

const initialState: IUSER = {
  userlist: [],
  loading: false,
};

export const userListReducer = (state = initialState, action: any): IUSER => {
  switch (action.type) {
    case 'SET_ALL_USER_LIST':
      return { ...state, userlist: action.value };
    default:
      return state;
  }
};
