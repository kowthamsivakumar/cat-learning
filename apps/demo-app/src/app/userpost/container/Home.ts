import React from 'react';
import { connect } from 'react-redux';
import { RootState } from '../common/store';
import Home from '../component/Home';
import {
  fetchTodoRequest,
  postData,
  getAlbumCount,
  getCommentCount,
  getPostCount,
  deletePost,
  getUserCount,
} from '../action/user';

const mapStateToProps = (state: RootState) => ({
  userPost: state.user,
});

const mapDispatchToProps = {
  fetchTodoRequest,
  getPostCount,
  postData,
  getAlbumCount,
  getCommentCount,
  getUserCount,
  deletePost,
};

export default connect(mapStateToProps, mapDispatchToProps)(Home);
