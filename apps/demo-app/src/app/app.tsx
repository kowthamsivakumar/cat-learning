import styles from './app.module.scss';
import { Menu, MenuItem, Sidebar, Icon, Segment } from 'semantic-ui-react';
import 'react-toastify/dist/ReactToastify.css';
import { ToastContainer, toast } from 'react-toastify';
import { Switch, Route, Link } from 'react-router-dom';
import Home from './userpost/container/Home';
import Counter from './userpost/component/Counter';
import GridType from './userpost/component/GridType';
import UserPost from './userpost/container/Userpost';
import { ReactComponent as Logo } from './logo.svg';
import star from './star.svg';
import SinglePost from './userpost/container/Singlepost';
import Users from './userpost/container/User';
import React, { useState } from 'react';

export function App(): JSX.Element {
  const [visible, setVisible] = useState(false);
  return (
    <>
      <Menu className="background-nav" style={{ marginBottom: 0 }}>
        <MenuItem>
          {' '}
          <Icon
            name="sidebar"
            onClick={() => {
              setVisible(!visible);
            }}
          />
        </MenuItem>
        <MenuItem>
          <Link to="/">Home</Link>
        </MenuItem>
        <MenuItem>
          <Link to="/counter">Counter</Link>
        </MenuItem>
        <MenuItem>Comments</MenuItem>
        <MenuItem>
          <Link to="/grid">Grid</Link>
        </MenuItem>
        <Menu.Menu position="right">
          <MenuItem>Logout</MenuItem>
        </Menu.Menu>
      </Menu>
      <Sidebar.Pushable>
        <Sidebar
          as={Menu}
          animation="overlay"
          icon="labeled"
          inverted
          onHide={() => setVisible(false)}
          vertical
          visible={visible}
          width="thin"
        >
          <Link to={'/'}>
            <Menu.Item as="a">
              <Icon name="home" />
              Home
            </Menu.Item>
          </Link>

          <Link to={'/users'}>
            <Menu.Item as="a">
              <Icon name="gamepad" />
              Users
            </Menu.Item>
          </Link>

          <Menu.Item as="a">
            <Icon name="camera" />
            Channels
          </Menu.Item>
        </Sidebar>

        <Sidebar.Pusher>
          <Switch>
            <Route path="/" component={Home} exact />
            <Route path="/counter" component={Counter} exact />
            <Route path="/grid" component={GridType} exact />
            <Route path="/user-post" component={UserPost} exact />
            <Route path="/post/:id" component={SinglePost} exact />
            <Route path="/users" component={Users} exact />
          </Switch>
        </Sidebar.Pusher>
      </Sidebar.Pushable>
      <ToastContainer />
    </>
  );
}

export default App;
