import React from "react";
import { Form, Input, Container, Button } from "semantic-ui-react";

function Todo(props: any) {
  return (
    <div className="ui items">
      {props.todo.map((s: any) => (
        <div className="ui message">
          <i className="close icon"></i>
          <div className="header">{s}</div>
        </div>
      ))}
    </div>
  );
}

export default Todo;
