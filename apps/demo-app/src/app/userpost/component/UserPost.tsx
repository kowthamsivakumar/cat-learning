import React, { useEffect, useState } from 'react';
import { USER } from '../types/UserPost';
import { Link } from 'react-router-dom';
import {
  Item,
  Menu,
  Modal,
  Button,
  Header,
  Form,
  Loader,
  Icon,
  Divider,
  Segment,
  Dimmer,
} from 'semantic-ui-react';
import { deletePost } from '../action/user';

interface IProps {
  userPost: USER;
  fetchTodoRequest: () => void;
  postData: (data: any) => void;
  deletePost: (id: number) => void;
  toggleModal: (data: boolean) => void;
}

const Userpost: React.FC<IProps> = ({
  userPost,
  fetchTodoRequest,
  postData,
  deletePost,
  toggleModal,
}) => {
  useEffect(() => fetchTodoRequest(), []);

  const [open, setOpen] = useState(false);
  const [postdata, setPostData] = useState({ title: '', body: '' });

  const handleOnChange = (key: string) => (event: any) => {
    setPostData({ ...postdata, [key]: event.target.value });
  };

  const onSubmit = () => {
    postData(postdata);
  };

  return (
    <>
      <div style={{ padding: '10px' }}>
        <div>
          <Menu text>
            <Menu.Item header>
              <Modal
                onClose={() => toggleModal(false)}
                onOpen={() => toggleModal(true)}
                open={userPost.toggleModal}
                trigger={
                  <Button>
                    <Icon name="add circle" /> ADD POST
                  </Button>
                }
              >
                <Modal.Header>ADD A POST</Modal.Header>
                <Modal.Content image>
                  <Modal.Description>
                    <Form>
                      <Form.Field>
                        <label>Title</label>
                        <input
                          placeholder="First Name"
                          type="text"
                          onChange={handleOnChange('title')}
                        />
                      </Form.Field>
                      <Form.TextArea
                        label="About"
                        placeholder="Tell us more about you..."
                        onChange={handleOnChange('body')}
                      />
                    </Form>
                  </Modal.Description>
                </Modal.Content>
                <Modal.Actions>
                  <Button color="black" onClick={() => toggleModal(false)}>
                    Nope
                  </Button>
                  {userPost.loading ? (
                    <Button loading primary>
                      Loading
                    </Button>
                  ) : (
                    <Button
                      content="Submit"
                      labelPosition="right"
                      icon="checkmark"
                      onClick={onSubmit}
                      positive
                    />
                  )}
                </Modal.Actions>
              </Modal>
            </Menu.Item>
          </Menu>
        </div>
        <Divider />
        <div style={{ height: '60vh', overflowY: 'auto', padding: '10px' }}>
          {userPost.loading ? (
            <Segment>
              <div
                style={{ height: '60vh', overflowY: 'auto', padding: '10px' }}
              >
                <Dimmer active inverted>
                  <Loader size="large">Loading</Loader>
                </Dimmer>
              </div>
            </Segment>
          ) : (
            <Item.Group divided>
              {userPost.userList.map((s) => (
                <Item key={s.id}>
                  <Item.Content>
                    <Item.Header as="a">{s.title}</Item.Header>
                    <Item.Meta>Description</Item.Meta>
                    <Item.Description>{s.body}</Item.Description>
                    <Item.Extra>
                      {' '}
                      <Link
                        to={`/post/${s.id}`}
                        className="button-primary"
                        style={{
                          padding: '10px 25px',
                          color: 'white',
                          borderRadius: '5px',
                        }}
                      >
                        View
                      </Link>
                      <Button
                        className="button-warning"
                        onClick={() => deletePost(s.id)}
                        primary
                      >
                        Delete
                      </Button>
                    </Item.Extra>
                  </Item.Content>
                </Item>
              ))}
            </Item.Group>
          )}
        </div>
      </div>
    </>
  );
};

export default Userpost;
