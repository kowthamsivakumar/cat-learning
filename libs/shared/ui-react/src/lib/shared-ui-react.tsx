import React from 'react';

import './shared-ui-react.module.scss';

/* eslint-disable-next-line */
export interface SharedUiReactProps {}

export function SharedUiReact(props: SharedUiReactProps) {
  return (
    <div>
      <h1>Welcome to shared-ui-react!</h1>
    </div>
  );
}

export default SharedUiReact;
