import * as Eff from 'redux-saga/effects';
import { toast } from 'react-toastify';
import {
  fetchList,
  postData,
  getAlbum,
  getComment,
  userCount,
  getpost,
  deletePostById,
} from '../request/userpost';
import {
  fetchTodoSuccess,
  getSinglePost,
  setAlbumCount,
  setCommentCount,
  setPostCount,
  setUserCount,
  setSinglePost,
  closeModal,
  toggleLoader,
} from '../../action/user';
const call: any = Eff.call;
export function* fetchAll(): any {
  console.log('in--------');
  try {
    yield Eff.put(toggleLoader());
    const response = yield Eff.call(fetchList);
    console.log('res', response);
    yield Eff.put(fetchTodoSuccess(response.data));
    yield Eff.put(setPostCount(response.data.length));
    yield Eff.put(toggleLoader());
  } catch (error) {
    yield Eff.put(toggleLoader());
    console.log('error', error);
  }
}

export function* getAlbumsCount(): any {
  console.log('in--------');
  try {
    const response = yield Eff.call(getAlbum);
    console.log('res', response);
    yield Eff.put(setAlbumCount(response.data.length));
  } catch (error) {
    console.log('error', error);
  }
}
export function* getCommentsCount(): any {
  console.log('in--------');
  try {
    const response = yield Eff.call(getComment);
    console.log('res', response);
    yield Eff.put(setCommentCount(response.data.length));
  } catch (error) {
    console.log('error', error);
  }
}

export function* getUsersCount(): any {
  console.log('in--------');
  try {
    const response = yield Eff.call(userCount);
    console.log('res', response);
    yield Eff.put(setUserCount(response.data.length));
  } catch (error) {
    console.log('error', error);
  }
}

export function* postUser(action: any): any {
  try {
    yield Eff.put(toggleLoader());
    const response: any = yield call(postData, action.value);
    console.log('res', response);
    yield Eff.put(closeModal(false));
    yield Eff.put(toggleLoader());
    toast.dark('Submited Successfully', {
      position: 'top-right',
      autoClose: 5000,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true,
      progress: undefined,
    });
  } catch (error) {
    yield Eff.put(toggleLoader());
    console.log('error', error);
  }
}

export function* deletePost(action: any): any {
  try {
    const response: any = yield call(deletePostById, action.id);
    console.log('res', response);
    toast.dark('Deleted Successfully', {
      position: 'top-right',
      autoClose: 5000,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true,
      progress: undefined,
    });
    yield call(fetchAll);
  } catch (error) {
    console.log('error', error);
  }
}

export function* setPost(action: any): any {
  try {
    const response: any = yield call(getpost, action.id);
    console.log('res', response);
    yield Eff.put(setSinglePost(response.data));
  } catch (error) {
    console.log('error', error);
  }
}

export function* ToggleModal(action: any): any {
  try {
    yield Eff.put(closeModal(action.data));
  } catch (error) {
    console.log('error', error);
  }
}

const GET_ALL_POST = 'GET_ALL_POST';
const POST_USER = 'POST_USER';

function* todoSaga() {
  yield Eff.all([
    Eff.takeEvery(GET_ALL_POST, fetchAll),
    Eff.takeEvery(POST_USER, postUser),
    Eff.takeEvery('GET_ALL_COMMENT_COUNT', getCommentsCount),
    Eff.takeEvery('GET_ALL_USER_COUNT', getUsersCount),
    Eff.takeEvery('GET_ALL_ALBUM_COUNT', getAlbumsCount),
    Eff.takeEvery('GET_SINGLE_POST', setPost),
    Eff.takeEvery('DELETE_POST', deletePost),
    Eff.takeEvery('TOGGLE_MODAL_CLOSE', ToggleModal),
  ]);
}

export default todoSaga;
